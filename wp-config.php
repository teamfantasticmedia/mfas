<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

ini_set('memory_limit','160M');

// ** MySQL settings - You can get this info from your web host ** //
if($_SERVER['SERVER_NAME'] == 'mfas') {		/*LOCAL*/
	
	define('DB_NAME', 'mfas');

	// define('DB_USER', 'root');
	// define('DB_PASSWORD', 'old-k2Wbge6');
	// define('DB_HOST', '192.168.0.25');

	/*toggle to this db if making updates to the site*/
	define('DB_USER', 'mfas-admin');
	define('DB_PASSWORD', 'eZio9!53');
	define('DB_HOST', '85.91.237.115');

	define('WP_SITEURL', 'http://mfas/');
	define('WP_HOME', 'http://mfas/');

}

elseif($_SERVER['SERVER_NAME'] == 'mfas.atfantastic.co.uk') {		/*DEVELOPMENT*/
	
	define('DB_NAME', 'mfas');
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'old-k2Wbge6');
	define('DB_HOST', '192.168.0.25');
	define('WP_SITEURL', 'http://mfas.atfantastic.co.uk/');
	define('WP_HOME', 'http://mfas.atfantastic.co.uk/');
	
}

elseif($_SERVER['SERVER_NAME'] == 'www.aqua-spritz.com') {			/*LIVE*/
	define('DB_NAME', 'mfas');
	define('DB_USER', 'mfas-admin');
	define('DB_PASSWORD', 'eZio9!53');
	define('DB_HOST', 'localhost');
	define('WP_SITEURL', 'http://www.aqua-spritz.com/');
	define('WP_HOME', 'http://www.aqua-spritz.com/');

}

elseif($_SERVER['SERVER_NAME'] == 'www.aqua-spritz.co.uk') {			/*LIVE*/
	define('DB_NAME', 'mfas');
	define('DB_USER', 'mfas-admin');
	define('DB_PASSWORD', 'eZio9!53');
	define('DB_HOST', 'localhost');
	define('WP_SITEURL', 'http://www.aqua-spritz.co.uk/');
	define('WP_HOME', 'http://www.aqua-spritz.co.uk/');

}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i4Zn;Mx@xQCG5jr7K%i-:Za8`JG`T3QM.LF(i:AOodP`%OPF,ULvNjfi`F_%$zSv');
define('SECURE_AUTH_KEY',  'wAcXxvZB%/!5l<[2=g%!PlR1zjSNW<6)Qw4]C;+9Q]!(mzu/)Af^-pb/e|tv|:MN');
define('LOGGED_IN_KEY',    'y[BG3=0cLx^r(o@;sttj_0vMJ@eN%#@{]%z$HmRZ;D~V|  c6g&m&N OVu/}[p$V');
define('NONCE_KEY',        '?fwF`pE#[ >.vf,h;&~#V ^yr~m!E*4QtWm[r0lre/Q~~86!UQFP^%z~0 UR5 43');
define('AUTH_SALT',        'b}>)hF(:!?7O(n>[fD}*bIC~Q~GB%g6|Kr3iN=B GxA|xFBx XRJ-A#N2#voB?@w');
define('SECURE_AUTH_SALT', 'aA_uYg9{=LRFTl5n*E=H(J-=mvvJec)2-tk<89ZDau0SmYvoF>Ayh9iornE)3Pmw');
define('LOGGED_IN_SALT',   'Q-+J)!Tn7m-nsTe9QI>ElH!,c_]Q8:) 2JojUpALT6h2VgT<tj$3`T6H{mfTOLt2');
define('NONCE_SALT',       '#?fw8+mM/Lh-V9`|TSG&$fukvkC[V ,R^h8a+9y>Is8~r= AuC$%Sitv/G9nnK5o');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
